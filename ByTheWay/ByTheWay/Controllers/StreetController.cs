﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Neo4jClient;
using Neo4jClient.Cypher;
using ByTheWay.Models;
using System.Collections;

namespace ByTheWay.Controllers
{
    public class StreetController : Controller
    {
        public GraphClient client;
        public static string oldName;
        public static string firstname;
        public static string oldMaxSpeed;
        public static string oldLenght;

        // GET: Street
        public ActionResult Street()
        {
            return View();
        }

        #region Zlatna pravila
        //******************** Z L A T N A      P R A V I L A ***************************************//
        //  pazimo i na smer strelice  i na ono sto vracamo  !!! U suprotnom moze da vrati null
        //  (n)<-[r:Ima_adresu]-(a) return n 		smer( <- vraca ulice, ako je suprotno  -> vraca adrese). A ako vracamo a onda je sve suportno


        // DIREKTNI UPITI U NEO4J BAZU
        //  start n = node(*) match(n) <-[r:Vodi_u]-(a) where a.name="KruzniTok"  return n //RADI
        //	start n = node(*) match(n) <-[r:Vodi_u]-(a)  where n.Name="Jugoslovenska" return a //RADI
        //  start n = node(*) match(n) <-[r: Vodi_u]-(a) where exists(a.name) and n.name="KruzniTok"  return a //RADI

        //Pronadji glumce u filmovima[pronadji adrese na osnovu ulice]
        //Pronadji filmove reditelja[pronadji ulice za raskrsnicu]
        //Pronadji glumce u filmovima reditelja[pronadji adrese u ulicama neke raskrsnice]
        #endregion

        public void prepareConnection()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "sifra");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }

        [HttpPost]
        public void addStreet(Street model)
        {
            prepareConnection();

            Street r = new Street();

            r.Length = model.Length;
            r.Name = model.Name;
            r.MaxSpeed = model.MaxSpeed;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", r.Name);
            queryDict.Add("Length", r.Length);
            queryDict.Add("MaxSpeed", r.MaxSpeed);

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Street {Name:'" + r.Name + "', Length:'" + r.Length + "', MaxSpeed:'" + r.MaxSpeed + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<Street> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).ToList();       
        }

        public void deleteStreet(Street model)
        {
            prepareConnection();

            string Name = model.Name.ToString();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Street) and exists(n.Name) and n.Name =~ {Name} delete n",
                                                            queryDict, CypherResultMode.Projection);

            List<Street> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).ToList();
        }

        public void changeStreet(Street model)
        {
            prepareConnection();

            string newName = model.Name.ToString();
            string newLenght = model.Length.ToString();
            string newMaxSpeed = model.MaxSpeed.ToString();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("newName", newName);
            queryDict.Add("newLenght", newLenght);
            queryDict.Add("newMaxSpeed", newMaxSpeed);

            queryDict.Add("oldName", oldName);
            queryDict.Add("oldLenght", oldLenght);
            queryDict.Add("oldMaxSpeed", oldMaxSpeed);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Street) and exists(n.Name) and n.Name =~ {oldName} set n.Name = {newName} return n",
                                                            queryDict, CypherResultMode.Set);

            List<Street> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).ToList();
        }

        public void oldStreet(Street model)
        {
            prepareConnection();
            oldName = model.Name.ToString();
            oldLenght = model.Length.ToString();
            oldMaxSpeed = model.MaxSpeed.ToString();
        }

        public String ReturnStreetByName(Street model)
        {
            prepareConnection();
            Street PronadjenaUlica = GetStreetByName(model.Name);
            String rezultat = "";
            rezultat = "Ime ulice: " + PronadjenaUlica.Name + " Maksimalna brzina u ulici: " + PronadjenaUlica.MaxSpeed + " Duzina ulice: " + PronadjenaUlica.Length + "metara";
            return rezultat;
        }

        public String GetStreet(Street model)
        {
            prepareConnection();
            Street r = new Street();
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Street) return n", new Dictionary<string, object>(), CypherResultMode.Set);
            List<Street> streets = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).ToList();
            String x = "";
            List<Street> z = new List<Models.Street>();
            foreach (Street s in streets)
            {
                 x =  x + " Name: "+ s.Name+" Length: "+s.Length+" MaxSpeed: "+s.MaxSpeed ;

                z.Add(s);
            }
            return x;
        }

        public Street GetStreetByName(String nazivUlice)
        {
            prepareConnection();
            String Name = nazivUlice;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Street) and n.Name =~{Name} return n", queryDict, CypherResultMode.Set);
            Street streetResult = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).FirstOrDefault();
            return streetResult;
        }

        public List<Adress> GetListAdressByStreet(String NazivUlice)
        {           
            String Name = ".*" + NazivUlice + ".*";
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);

            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r: Ima_adresu]->(a) where exists(n.Name) and n.Name=~{Name}  return a",
                                                              queryDict, CypherResultMode.Set);
        
            List<Adress> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Adress>(query).ToList();
            return adrese;
        }


        public List<Intersection> GetListIntersectionByStreetVodiUVeza(String NazivUlice)
        { 
            String Name = NazivUlice;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);

            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r:Vodi_u]-(a)  where n.Name=~{Name} return a",
                                                              queryDict, CypherResultMode.Set);

            List<Intersection> raskrsniceZaUlicu = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
            return raskrsniceZaUlicu;
        }

        public List<Intersection> GetListIntersectionByStreetIzlaziNaVeza(String NazivUlice)
        {
            String Name = NazivUlice;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);

            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Izlazi_na]->(a)  where n.Name=~{Name} return a",
                                                              queryDict, CypherResultMode.Set);

            List<Intersection> raskrsniceZaUlicu = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
            return raskrsniceZaUlicu;
        }

        public String returnAllObjectsForStreet(Street model)
        {
            prepareConnection();
            String rezultat = "";
            List<Intersection> raskrsniceZaUlicu = GetListIntersectionByStreetIzlaziNaVeza(model.Name);
            foreach (Intersection s in raskrsniceZaUlicu)
            {
                rezultat = rezultat + " Name: " + s.name;
                rezultat += Environment.NewLine;
            }
            List<Adress> adrese = GetListAdressByStreet(model.Name);
            foreach (Adress a in adrese)
            {
                rezultat = rezultat + " Number: " + a.Number;
                rezultat += Environment.NewLine;
            }
            return rezultat;
        }

        public List<Adress> GetAdressByIntersectionThrowStreet(String NazivRaskrsnice)//Kombinovani upit
        {
            prepareConnection();
            String IntersectionName = NazivRaskrsnice;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("IntersectionName", IntersectionName);
            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Vodi_u]->(a)-[r1:Ima_adresu]->(m) where n.name=~{IntersectionName} return m",
                                                              queryDict, CypherResultMode.Set);
            List<Adress> adreseUlice = ((IRawGraphClient)client).ExecuteGetCypherResults<Adress>(query).ToList();
            return adreseUlice;
        }

        public String GetAdressByIntersectionThrowStreetString(Street model)
        {
            prepareConnection();
            List<Adress> adreseUlice = GetAdressByIntersectionThrowStreet(model.Name);
            String rezultat="";
            foreach (Adress a in adreseUlice)
            {
                rezultat = rezultat + " Adresa br: " + a.Number;
                rezultat += Environment.NewLine;
            }
            return rezultat;
        }

        public String findCommonStreet(Street model) // za unete adrese vraca putanja ALI SAMO U SLUCAJU AKO SU ADRESE RAZDVOJENE NAJVISE JEDNOM RASKRSNICOM 
        {
            prepareConnection();
            String rezultat = "Nije pronadjen put";
            String Number1 = model.Name;
            String Number2 = model.Length.ToString();
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Adress1", Number1);
            queryDict.Add("Adress2", Number2);

            var queryForStreet1 = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r: Ima_adresu]-(a) where exists(n.Number) and n.Number=~{Adress1}  return a", queryDict, CypherResultMode.Set);
            var queryForStreet2 = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r: Ima_adresu]-(a) where exists(n.Number) and n.Number=~{Adress2}  return a", queryDict, CypherResultMode.Set);
            List<Street> nadjenaUlica1 = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(queryForStreet1).ToList();
            List<Street> nadjenaUlica2 = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(queryForStreet2).ToList();
            foreach (Street nadjenaU1 in nadjenaUlica1)
            {
                foreach (Street nadjenaU2 in nadjenaUlica2)
                {
                    if (nadjenaU1.Name == nadjenaU2.Name)
                    {
                        rezultat = "Adrese " + Number1 + " i " + Number2 + " se nalaze u istoj ulici sa imenom: " + nadjenaU1.Name;
                        return rezultat;
                    }
                }

            }

            var RetrurnedIntersection1 = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r1:Ima_adresu]-(a)-[r:Izlazi_na]->(c) where n.Number=~{Adress1}  return c ", queryDict, CypherResultMode.Set);
            var RetrurnedIntersection2 = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r1:Ima_adresu]-(a)-[r:Izlazi_na]->(c) where n.Number=~{Adress2}  return c ", queryDict, CypherResultMode.Set);
            List<Intersection> ZaPrvuAdresuRaskrsnice = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(RetrurnedIntersection1).ToList();
            List<Intersection> ZaDruguAdresuRaskrsnice = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(RetrurnedIntersection2).ToList();
            String ZajednickaRaskrsnica = "";
            foreach (Intersection r1 in ZaPrvuAdresuRaskrsnice)
            {
                foreach (Intersection r2 in ZaDruguAdresuRaskrsnice)
                {
                    if (r1.name == r2.name)
                        ZajednickaRaskrsnica = r1.name;
                }


            }
            if (ZajednickaRaskrsnica != "")
            {
                String IntersectionName = ZajednickaRaskrsnica;
                queryDict.Add("IntersectionName", IntersectionName);
                var VratiOdgovarajuceUlice = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r:Izlazi_na]-(a)-[r1:Ima_adresu]->(m) where n.name=~{IntersectionName} and m.Number=~{Adress1} or m.Number=~{Adress2} return a", queryDict, CypherResultMode.Set);
                List<Street> putanjaStreet = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(VratiOdgovarajuceUlice).ToList();

                rezultat = "Putanja od adrese: " + Number1 + " do " + Number2 + " je " + " od ulice " + putanjaStreet[0].Name + " kroz raskrsnicu " + ZajednickaRaskrsnica + " do ulice " + putanjaStreet[1].Name + " i tamo ste";
            }
            return rezultat;
        }
    }
}