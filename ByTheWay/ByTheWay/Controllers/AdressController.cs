﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Neo4jClient;
using Neo4jClient.Cypher;
using ByTheWay.Models;
using ByTheWay.Controllers;

namespace ByTheWay.Controllers
{
    public class AdressController : Controller
    {
        public GraphClient client;
        public static string oldNumber;
        public static string oldDistance;

        // GET: Adress
        public ActionResult Index()
        {
            return View();
        }

        public void prepareConnection()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "sifra");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }

        [HttpPost]
        public void addAdress(Adress model)
        {
            prepareConnection();

            Adress a = new Adress();

            a.Number = model.Number;
            a.Distance = model.Distance;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Number", a.Number);
            queryDict.Add("Distance", a.Distance);

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Adress {Number:'" + a.Number + "', Distance:'" + a.Distance + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<Adress> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Adress>(query).ToList();
        }

        public void deleteAdress(Adress model)
        {
            prepareConnection();

            string distance = model.Distance.ToString();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Distance", distance);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Adress) and exists(n.Distance) and n.Distance =~ {Distance} delete n",
                                                            queryDict, CypherResultMode.Projection);

            List<Adress> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Adress>(query).ToList();
        }

        public void changeAdress(Adress model)
        {
            prepareConnection();

            string newDistance = model.Distance.ToString();
            string newNumber = model.Number.ToString();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("newDistance", newDistance);
            queryDict.Add("newNumber", newNumber);
            queryDict.Add("oldDistance", oldDistance);
            queryDict.Add("oldNumber", oldNumber);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Adress) and exists(n.Distance) and n.Distance =~ {oldDistance} and exists(n.Number) and n.Number =~ {oldNumber} set n.distance = {newDistance} set n.Number = {newNumber} return n",
                                                            queryDict, CypherResultMode.Set);

            List<Adress> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Adress>(query).ToList();
        }

        public void oldAdress(Adress model)
        {
            prepareConnection();
            oldDistance = model.Distance.ToString();
            oldNumber = model.Number.ToString();
        }
        public String GetAdress(Adress model)
        {
            prepareConnection();
            Adress r = new Adress();
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Adress) return n", new Dictionary<string, object>(), CypherResultMode.Set);
            List<Adress> adress = ((IRawGraphClient)client).ExecuteGetCypherResults<Adress>(query).ToList();
            String x = "";
            List<Adress> z = new List<Models.Adress>();
            foreach (Adress s in adress)
            {
                x = x + " Distance:" + s.Distance + " Number:" + s.Number;
                z.Add(s);
            }
            return x;
        }
        
        public String getStreetByAdress(Adress model)
        {
            prepareConnection();
            String number = model.Number;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("number", number);
            var queryForAdressInformation = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Adress) and n.Number=~{number} return n ", queryDict, CypherResultMode.Set);
            var queryForStreet = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r: Ima_adresu]-(a) where exists(n.Number) and n.Number=~{number}  return a", queryDict, CypherResultMode.Set);
            Adress PodaciOAdresi= ((IRawGraphClient)client).ExecuteGetCypherResults<Adress>(queryForAdressInformation).FirstOrDefault();
            Street nadjenaUlica = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(queryForStreet).FirstOrDefault();

            String rezultat = "Adresa: " + model.Number.ToString() + " nalazi se u ulici " + nadjenaUlica.Name + " na rastojanju od " + PodaciOAdresi.Distance.ToString() + "m";
            return rezultat;
        }

        public List<Street> getAllStreets(String adrNumber)
        {
            prepareConnection();
            String number = adrNumber;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("number", number);   
            var queryForStreet = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r: Ima_adresu]-(a) where exists(n.Number) and n.Number=~{number}  return a", queryDict, CypherResultMode.Set);
            List<Street> sveUlice = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(queryForStreet).ToList();
            return sveUlice;
        }





        /*
        public String findShortestPath(Adress model)
        {
             - nema sanse, mnogo rekurzija treba
            prepareConnection();
            String result = "";
            String Number1 = model.Number;
            String Number2 = model.Distance.ToString();
            List<Street> uliceZaPrvuAdresu = getAllStreets(Number1);
            foreach (Street st in uliceZaPrvuAdresu)
            {
                List<Adress> adrese = new StreetController().GetListAdressByStreet(st.Name);
                foreach (Adress adr in adrese)
                {
                    if(adr.Number==Number2)
                    {
                        //nasao sam ga sad da se sakupe sve distance
                    }
                }
                List<Intersection> raskrsniceZaUlicuIzlaziNa = new StreetController().GetListIntersectionByStreetIzlaziNaVeza(st.Name);
                List<Intersection> raskrsniceZaUlicuVodiU = new StreetController().GetListIntersectionByStreetVodiUVeza(st.Name);

                foreach (Intersection its1 in raskrsniceZaUlicuIzlaziNa)
                {
                    List<Street> streetList1 = new IntersectionController().getAllStreetsVodiU(its1.name);
                    List<Street> streetList2 = new IntersectionController().getAllStreetsIzlaziNa(its1.name);
                }
            }            
        }
        */
    }
}