﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Neo4jClient;
using Neo4jClient.Cypher;
using ByTheWay.Models;

namespace ByTheWay.Controllers
{
    public class RoadController : Controller
    {

        public GraphClient client;
        public static string oldName;
        public static string oldMaxSpeed;
        public static string oldLenght;

        // GET: Road
        public ActionResult Road()
        {
            return View();
        }

        public void prepareConnection()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "sifra");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }

        [HttpPost]
        public void addRoad(Road model)
        {
            prepareConnection();

            Road r = new Road();

            r.Length = model.Length;
            r.Name = model.Name;
            r.MaxSpeed = model.MaxSpeed;

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", r.Name);
            queryDict.Add("Length", r.Length);
            queryDict.Add("MaxSpeed", r.MaxSpeed);

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Road {Name:'" + r.Name + "', Length:'" + r.Length + "', MaxSpeed:'" + r.MaxSpeed + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<Road> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Road>(query).ToList();
        }

        public void deleteRoad(Road model)
        {
            prepareConnection();

            string Name = model.Name.ToString();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Road) and exists(n.Name) and n.Name =~ {Name} delete n",
                                                            queryDict, CypherResultMode.Projection);

            List<Road> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Road>(query).ToList();
        }
        public void changeRoad(Road model)
        {
            prepareConnection();

            string newName = model.Name.ToString();
            string newLenght = model.Length.ToString();
            string newMaxSpeed = model.MaxSpeed.ToString();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("newName", newName);
            queryDict.Add("newLenght", newLenght);
            queryDict.Add("newMaxSpeed", newMaxSpeed);

            queryDict.Add("oldName", oldName);
            queryDict.Add("oldLenght",oldLenght);
            queryDict.Add("oldMaxSpeed", oldMaxSpeed);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Road) and exists(n.Name) and n.Name =~ {oldName} set n.Name = {newName} return n",
                                                            queryDict, CypherResultMode.Set);

            List<Road> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Road>(query).ToList();
        }

        public void oldRoad(Road model)
        {
            prepareConnection();
            oldName = model.Name.ToString();
            oldLenght = model.Length.ToString();
            oldMaxSpeed = model.MaxSpeed.ToString();
        }

        public String GetRoad(Road model)
        {
            prepareConnection();
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Road) return n", new Dictionary<string, object>(), CypherResultMode.Set);
            List<Road> roads = ((IRawGraphClient)client).ExecuteGetCypherResults<Road>(query).ToList();
            String x = "";
            foreach (Road s in roads)
            {
                x =   x + " Name: " + s.Name + " Length: " + s.Length + " MaxSpeed: " + s.MaxSpeed;
            }
            return x;
        }
        public String GetRoadByName(Road model)
        {
            prepareConnection();
            Road r = new Road();
            string Name = ".*"+model.Name+".*";
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Road) and exists(n.Name) and n.Name =~ {Name} return n", queryDict, CypherResultMode.Set);
            Road returnRoad = ((IRawGraphClient)client).ExecuteGetCypherResults<Road>(query).FirstOrDefault();
            String Rezultat = "Naziv puta: " + returnRoad.Name + " maksimalna brzina " + returnRoad.MaxSpeed + " duzina puta " + returnRoad.Length + "m";
            return Rezultat;
        }

        public String GetAllRoads(Road model)
        {
            prepareConnection();
            String Name = model.Name;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);

            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Povezan_sa]->(a) where exists(n.Name) and n.Name=~{Name} return a",
                                                              queryDict, CypherResultMode.Set);
            String x = "";
            List<Road> roads = ((IRawGraphClient)client).ExecuteGetCypherResults<Road>(query).ToList();
            foreach (Road rr in roads)
            {
                x = x + " Name: " + rr.Name + " Max Speed: " + rr.MaxSpeed + " Length " + rr.Length;
                x += Environment.NewLine;
            }

            var query1 = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Izlazi_na]->(a) where exists(n.Name) and n.Name=~{Name} return a",
                                                              queryDict, CypherResultMode.Set); 

            List<Intersection> intersections = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query1).ToList();
            foreach (Intersection rr in intersections)
            {
                x = x + " Name: " + rr.name;
                x += Environment.NewLine;
            }
            return x;
        }

        public String GetIntersectionByRoadName(Road model)
        {
            prepareConnection();
            String rezultat = model.Name + " izlazi na sledece raskrsnice: ";
            String Name = model.Name;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);

            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Izlazi_na]->(a)  where n.Name=~{Name} return a",
                                                              queryDict, CypherResultMode.Set);

            List<Intersection> raskrsniceZaUlicu = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
            foreach (Intersection i in raskrsniceZaUlicu)
            {
                rezultat = rezultat + i.name + " ";
            }
            return rezultat;
        }
    }
}