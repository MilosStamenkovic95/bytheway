﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Neo4jClient;
using Neo4jClient.Cypher;
using ByTheWay.Models;

namespace ByTheWay.Controllers
{
    public class IntersectionController : Controller
    {
        public GraphClient client;
        public static string oldName;

        // GET: Intersection
        public ActionResult Intersection()
        {
            return View();
        }
        public void prepareConnection()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "sifra");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }
        [HttpPost]
        public void addIntersection(Intersection inter)
        {
            prepareConnection();

            Intersection i = new Intersection();

            i.name = inter.name;

   
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("name", i.name);


            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Intersection {name:'" + i.name + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<Intersection> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
        }

        public void deleteIntersection(Intersection inter)
        {
            prepareConnection();

            string name = inter.name.ToString();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("name", name);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) and exists(n.name) and n.name =~ {name} delete n",
                                                            queryDict, CypherResultMode.Projection);

            List<Intersection> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
        }

        public void changeIntersection(Intersection model)
        {
            prepareConnection();

            string newName = model.name.ToString();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("newName", newName);
            queryDict.Add("oldName", oldName);

            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) and exists(n.name) and n.name =~ {oldName}  set n.name = {newName}  return n",
                                                            queryDict, CypherResultMode.Set);

            List<Intersection> adrese = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
        }

        public void oldIntersection(Intersection model)
        {
            prepareConnection();
            oldName = model.name.ToString();
        }
        public String GetIntersection(Intersection model)
        {
            prepareConnection();
            Intersection r = new Intersection();
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) return n", new Dictionary<string, object>(), CypherResultMode.Set);
            List<Intersection> intersections = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
            String x = "";
            List<Intersection> z = new List<Models.Intersection>();
            foreach (Intersection s in intersections)
            {
                x = x + " Name: " + s.name;
                z.Add(s);
            }
            return x;
        }

        public String GetAllIntersections(Intersection model)
        {
            prepareConnection();
            String Name = model.name;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);
            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r:Vodi_u]-(a) where exists(a.name) and a.name=~{Name} return n",
                                                              queryDict, CypherResultMode.Set);
            String x = "Podaci o ulicama: ";
            List<Street> UliceNaRaskrsnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).ToList();
            
            foreach (Street str in UliceNaRaskrsnici)
            {
                x = x + " Naziv ulice: " + str.Name + " maksimalna brzina: " + str.MaxSpeed + " duzina " + str.Length;
                x += Environment.NewLine;
            }
            var query1 = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Vodi_na]->(a) where n.name=~{Name} return a",
                                                              queryDict, CypherResultMode.Set);

            x = x + " Podaci o putevima: ";

            List<Road> putevi = ((IRawGraphClient)client).ExecuteGetCypherResults<Road>(query1).ToList();
            foreach(Road rd in putevi)
            {
                x = x + " Naziv puta: " + rd.Name + " maksimalna brzina: " + rd.MaxSpeed + " duzina " + rd.Length;
                x += Environment.NewLine;
            }
            return x;
        }

        public List<Street> getAllStreetsVodiU(String intersectionName)
        {
            prepareConnection();
            String Name = intersectionName;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);
            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) <-[r:Vodi_u]-(a) where exists(a.name) and a.name=~{Name} return n",
                                                              queryDict, CypherResultMode.Set);
            List<Street> UliceNaRaskrsnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).ToList();
            return UliceNaRaskrsnici;
        }

        public List<Street> getAllStreetsIzlaziNa(String intersectionName)
        {
            prepareConnection();
            String Name = intersectionName;
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Name", Name);
            var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Izlazi_na]->(a) where exists(a.name) and a.name=~{Name} return n",
                                                              queryDict, CypherResultMode.Set);
            List<Street> UliceNaRaskrsnici = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).ToList();
            return UliceNaRaskrsnici;
        }
    }
}