﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ByTheWay.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            
            return View();
        }
        public ActionResult Intersection()
        {
            return View();


        }
        public ActionResult Road()
        {
            return View();


        }
        public ActionResult Street()
        {
            return View();


        }

        public ActionResult Admin()
        {
            return View();
        }

        public ActionResult Adress()
        {
            return View();
        }

        public ActionResult Connections()
        {
            return View();
        }

        public ActionResult UserPage()
        {
            return View();
        }
    }

}