﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Neo4jClient;
using Neo4jClient.Cypher;
using ByTheWay.Models;

namespace ByTheWay.Controllers
{
    public class UnicController : Controller
    {
        public GraphClient client;
        public static string firstname;
        public static string firstType;

        // GET: Street
        public ActionResult Street()
        {
            return View();
        }

        public void prepareConnection()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "sifra");

            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }

        private void CreateUniqueRelationship(string Name1, string Name2 , bool twoWay, GraphClient client, string firstType, string secondType)
        {
            if (firstType == "Street")
            {
                if (secondType == "Adress")
                {
                    var query = client.Cypher
                    .Match("(p1:" + firstType + ")", "(p2:" + secondType + ")")
                    .Where((Street p1) => p1.Name == Name1)
                    .AndWhere((Adress p2) => p2.Number == Name2)
                    .CreateUnique("(p1)-[:Ima_adresu]->(p2)");
                    query.ExecuteWithoutResults();
                }
                else if (secondType == "Intersection")
                {
                    var query = client.Cypher
                    .Match("(p1:" + firstType + ")", "(p2:" + secondType + ")")
                    .Where((Street p1) => p1.Name == Name1)
                    .AndWhere((Intersection p2) => p2.name == Name2)
                    .CreateUnique("(p1)-[:Izlazi_na]->(p2)");
                    query.ExecuteWithoutResults();
                }
            }
            else if (firstType == "Road")
            {
                if (secondType == "Road")
                {
                    var query = client.Cypher
                    .Match("(p1:" + firstType + ")", "(p2:" + secondType + ")")
                    .Where((Road p1) => p1.Name == Name1)
                    .AndWhere((Road p2) => p2.Name == Name2)
                    .CreateUnique("(p1)-[:Povezan_sa]->(p2)");
                    query.ExecuteWithoutResults();
                }
                else if (secondType == "Intersection")
                {
                    var query = client.Cypher
                    .Match("(p1:" + firstType + ")", "(p2:" + secondType + ")")
                    .Where((Road p1) => p1.Name == Name1)
                    .AndWhere((Intersection p2) => p2.name == Name2)
                    .CreateUnique("(p1)-[:Izlazi_na]->(p2)");
                    query.ExecuteWithoutResults();
                }
            }
            else if (firstType == "Intersection")
            {
                if (secondType == "Road")
                {
                    var query = client.Cypher
                    .Match("(p1:" + firstType + ")", "(p2:" + secondType + ")")
                    .Where((Intersection p1) => p1.name == Name1)
                    .AndWhere((Road p2) => p2.Name == Name2)
                    .CreateUnique("(p1)-[:Vodi_na]->(p2)");
                    query.ExecuteWithoutResults();
                }
                else if (secondType == "Street")
                {
                    var query = client.Cypher
                    .Match("(p1:" + firstType + ")", "(p2:" + secondType + ")")
                    .Where((Intersection p1) => p1.name == Name1)
                    .AndWhere((Street p2) => p2.Name == Name2)
                    .CreateUnique("(p1)-[:Vodi_u]->(p2)");
                    query.ExecuteWithoutResults();
                }
            }
        }

        public void CreateEdge(UniModel model)
        {
            prepareConnection();

            string secondType = model.secondItem.ToString();
            string Name2 = model.ID2.ToString();
            bool twoWay = true;

            CreateUniqueRelationship(firstname, Name2, twoWay, client, firstType, secondType);
        }

        public void firstStreet(UniModel model)
        {
            prepareConnection();
            firstname = model.ID1.ToString();
            firstType = model.firstItem.ToString();
        }

        public void fillLists()
        {
            prepareConnection();
            Street st = new Street();
            Adress ad = new Adress();
            Road rd = new Road();
            Intersection ist = new Intersection();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Street) return n", new Dictionary<string, object>(), CypherResultMode.Set);
            var query1 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Road) return n", new Dictionary<string, object>(), CypherResultMode.Set);
            var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Adress) return n", new Dictionary<string, object>(), CypherResultMode.Set);
            var query3 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) return n", new Dictionary<string, object>(), CypherResultMode.Set);

            List<Street> streets = ((IRawGraphClient)client).ExecuteGetCypherResults<Street>(query).ToList();
            List<Road> roads = ((IRawGraphClient)client).ExecuteGetCypherResults<Road>(query1).ToList();
            List<Adress> adresses = ((IRawGraphClient)client).ExecuteGetCypherResults<Adress>(query2).ToList();
            List<Intersection> intersections = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query3).ToList();

            UniModel.allStreets = new List<Models.Street>();
            UniModel.allRoads = new List<Models.Road>();
            UniModel.allAdresses = new List<Models.Adress>();
            UniModel.allIntersections = new List<Models.Intersection>();

            foreach (Street s in streets)
            {
                UniModel.allStreets.Add(s);
            }
            foreach (Road r in roads)
            {
                UniModel.allRoads.Add(r);
            }
            foreach (Adress a in adresses)
            {
                UniModel.allAdresses.Add(a);
            }
            foreach (Intersection i in intersections)
            {
                UniModel.allIntersections.Add(i);
            }
            writeToJSON();
        }

        public void writeToJSON()
        {
            prepareConnection();
            string rootPath = AppDomain.CurrentDomain.BaseDirectory;
            string path = @"" + rootPath + "Example.json";
            using (var tw = new StreamWriter(path, false))
            {
                tw.WriteLine("{");
                tw.WriteLine("\t" + "\"" + "nodes" + "\"" + ": [");
                foreach (Intersection its in UniModel.allIntersections)
                {
                    Dictionary<string, object> queryDict = new Dictionary<string, object>();
                    queryDict.Add("Name", its.name);
                    var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) and exists(n.name) and n.name =~{Name} return id(n)",
                                                            queryDict, CypherResultMode.Set);
                    String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

                    Intersection last = UniModel.allIntersections.Last();
                    tw.WriteLine("\t" + "\t" + "{");
                    tw.WriteLine("\t" + "\t" + "\t" + "\"" + "caption" + "\"" + ": " + "\"" + its.name + "\"" + ",");
                    tw.WriteLine("\t" + "\t" + "\t" + "\"" + "type" + "\"" + ": " + "" + "\"" + "\"" + ",");
                    tw.WriteLine("\t" + "\t" + "\t" + "\"" + "id" + "\"" + ": " + maxId);
                    if (its.Equals(last))
                    {
                        tw.WriteLine("\t" + "\t" + "}");
                    }
                    else
                    {
                        tw.WriteLine("\t" + "\t" + "},");
                    }
                }
                tw.WriteLine("\t" + "],");
                tw.WriteLine("\t" + "\"" + "edges" + "\"" + ": [");

                foreach (Street str in UniModel.allStreets)
                {
                    string tBox = str.Name;
                    Dictionary<string, object> queryDict = new Dictionary<string, object>();
                    queryDict.Add("Name", tBox);
                    var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Izlazi_na]->(a) where exists(n.Name) and n.Name=~{Name} return a",
                                                                      queryDict, CypherResultMode.Set);
                    List<Intersection> raskrsniceZaUlicu = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
                    if (raskrsniceZaUlicu.Count == 2)
                    {
                        Dictionary<string, object> queryDict1 = new Dictionary<string, object>();
                        queryDict1.Add("Name", raskrsniceZaUlicu[0].name);
                        Dictionary<string, object> queryDict2 = new Dictionary<string, object>();
                        queryDict2.Add("Name", raskrsniceZaUlicu[1].name);

                        var query1 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) and exists(n.name) and n.name =~{Name} return id(n)",
                                                                queryDict1, CypherResultMode.Set);
                        var query2 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) and exists(n.name) and n.name =~{Name} return id(n)",
                                                                queryDict2, CypherResultMode.Set);

                        String maxId1 = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query1).ToList().FirstOrDefault();
                        String maxId2 = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query2).ToList().FirstOrDefault();

                        tw.WriteLine("\t" + "\t" + "{");
                        tw.WriteLine("\t" + "\t" + "\t" + "\"" + "source" + "\"" + ": " + maxId1 + ",");
                        tw.WriteLine("\t" + "\t" + "\t" + "\"" + "target" + "\"" + ": " + maxId2 + ",");
                        tw.WriteLine("\t" + "\t" + "\t" + "\"" + "caption" + "\"" + ": " + "\"" + str.Name + "\"");
                        tw.WriteLine("\t" + "\t" + "},");
                    }                  
                }

                foreach (Road rd in UniModel.allRoads)
                {
                    Road last = UniModel.allRoads.Last();
                    string tBox = rd.Name;
                    Dictionary<string, object> queryDict = new Dictionary<string, object>();
                    queryDict.Add("Name", tBox);

                    var query = new Neo4jClient.Cypher.CypherQuery("start n = node(*) match(n) -[r:Izlazi_na]->(a) where exists(n.Name) and n.Name=~{Name} return a",
                                                                      queryDict, CypherResultMode.Set);
                    List<Intersection> intersections = ((IRawGraphClient)client).ExecuteGetCypherResults<Intersection>(query).ToList();
                    if (intersections.Count == 2)
                    {
                        Dictionary<string, object> queryDict3 = new Dictionary<string, object>();
                        queryDict3.Add("Name", intersections[0].name);
                        Dictionary<string, object> queryDict4 = new Dictionary<string, object>();
                        queryDict4.Add("Name", intersections[1].name);

                        var query3 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) and exists(n.name) and n.name =~{Name} return id(n)",
                                                                queryDict3, CypherResultMode.Set);
                        var query4 = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Intersection) and exists(n.name) and n.name =~{Name} return id(n)",
                                                                queryDict4, CypherResultMode.Set);

                        String maxId3 = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query3).ToList().FirstOrDefault();
                        String maxId4 = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query4).ToList().FirstOrDefault();

                        tw.WriteLine("\t" + "\t" + "{");
                        tw.WriteLine("\t" + "\t" + "\t" + "\"" + "source" + "\"" + ": " + maxId3 + ",");
                        tw.WriteLine("\t" + "\t" + "\t" + "\"" + "target" + "\"" + ": " + maxId4 + ",");
                        tw.WriteLine("\t" + "\t" + "\t" + "\"" + "caption" + "\"" + ": " + "\"" + rd.Name + "\"");
                        if (rd.Equals(last))
                        {
                            tw.WriteLine("\t" + "\t" + "}");
                        }
                        else
                        {
                            tw.WriteLine("\t" + "\t" + "},");
                        }
                    }
                }

                tw.WriteLine("\t" + "]");
                tw.WriteLine("}");
                tw.Close();
            }
        }

    }
}