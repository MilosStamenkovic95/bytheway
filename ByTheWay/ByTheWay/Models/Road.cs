﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByTheWay.Models
{
    public class Road : Way
    {
        public int MaxSpeed { get; set; }
        public float Length { get; set; }
        public string Name { get; set; }
        public List<Intersection> Connections;
        public List<Road> RoadLinks;

        public Road()
        {
            Connections = new List<Intersection>();
            RoadLinks = new List<Road>();
        }
    }
}