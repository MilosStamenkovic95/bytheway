﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByTheWay.Models
{
    public class UniModel
    {
        public string firstItem { get; set; }
        public string secondItem { get; set; }
        public string ID1 { get; set; }
        public string ID2 { get; set; }

        public static List<Road> allRoads { get; set; }
        public static List<Street> allStreets { get; set; }
        public static List<Intersection> allIntersections { get; set; }
        public static List<Adress> allAdresses { get; set; }
    }
}