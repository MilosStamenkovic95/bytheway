﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByTheWay.Models
{
    public interface Way
    {
        int MaxSpeed { get; set; }
        float Length { get; set; }
        string Name { get; set; }
    }
}
