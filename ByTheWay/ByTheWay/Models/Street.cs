﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ByTheWay.Models
{
    public class Street : Way
    {
        public int MaxSpeed { get; set; }
        public float Length { get; set; }
        public string Name { get; set; }
        public List<Adress> Adresses;
        public List<Intersection> Intersections;

        public Street()
        {
            Adresses = new List<Adress>();
            Intersections = new List<Intersection>();
        }
    }
}